-- Which five movies were rented more than the others, and what is the expected age of the audience for these movies?

SELECT
    film_id,
    title,
    COUNT(rental_id) AS rental_count,
    AVG(DATE_PART('year', current_date) - DATE_PART('year', release_year)) AS expected_age
FROM film
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY film_id, title, release_year
ORDER BY rental_count DESC
LIMIT 5;