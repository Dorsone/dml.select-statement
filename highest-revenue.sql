-- Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?

SELECT 
    store_id,
    staff_id,
    MAX(total_revenue) AS highest_revenue
FROM (
    SELECT 
        store_id,
        staff_id,
        DATE_PART('year', payment_date) AS payment_year,
        SUM(amount) AS total_revenue
    FROM payment
    WHERE DATE_PART('year', payment_date) = 2017
    GROUP BY store_id, staff_id, payment_year
) AS revenue_by_staff_and_store
GROUP BY store_id, staff_id
ORDER BY store_id, highest_revenue DESC;
